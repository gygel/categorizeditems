package by.bylinay.trening.categorizedItems.expensesBalancer;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.math.BigDecimal;

@Getter
@AllArgsConstructor
public class TransactionValue {
    BigDecimal amount;

    public TransactionValue(TransactionValue value) {
        this.amount = value.getAmount();
    }


    public void add(TransactionValue value) {
        this.amount = amount.add(value.getAmount());
    }


}
