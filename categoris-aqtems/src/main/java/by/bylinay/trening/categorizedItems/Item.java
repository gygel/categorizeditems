package by.bylinay.trening.categorizedItems;

import by.bylinay.trening.categorizedItems.expensesBalancer.TransactionValue;

import java.math.BigDecimal;

public interface Item extends Entity {

    int getId();

    String getName();

    int getCategoryId();

    Category getCategory();

    TransactionValue getTransactionValue();

    // TODO is this needed?
    int getManeyInСents();

}
