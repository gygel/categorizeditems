package by.bylinay.trening.categorizedItems.expensesBalancer;


import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import by.bylinay.trening.categorizedItems.Category;
import by.bylinay.trening.categorizedItems.Item;


public class ItemBalancer {

    public Map<Category, Priority> getPriorities(Map<Category, Integer> targets, List<Item> items) {
        final Map<Category, Priority> priorities = new HashMap<>();
        final Map<Category, TransactionValue> totals = new HashMap<>();

        for (Item item : items) {
            Category category = item.getCategory();
            Integer target = targets.get(category);
            TransactionValue total = totals.get(category);
            if (total == null) {
                totals.put(category, new TransactionValue(item.getTransactionValue()));
            } else {
                total.add(item.getTransactionValue());
            }
        }

        for (Entry<Category, Integer> targetEntry : targets.entrySet()) {
            Category category = targetEntry.getKey();
            Integer target = targetEntry.getValue();
            TransactionValue total = totals.get(category);
            priorities.put(category, new Priority(target, total));
        }

        return priorities;
    }

}
